const Path = require('path');
const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');

const defaultContext = {
    title: 'Arcadia Theme',
};

const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'public'),
            },
        },
    },
});

server.connection({
    port: 80,
    host: 'localhost',
});

server.register(Inert, () => {});
server.register(Vision, () => {});

server.views({
    engines: {
        html: require('handlebars'),
    },
    relativeTo: __dirname,
    layout: true,
    path: 'views',
    partialsPath: 'views/partials',
    layoutPath: 'views/layouts',
    helpersPath: 'views/helpers',
    context: defaultContext,
});

server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: '.',
            redirectToSlash: true,
            index: true,
        },
    },
});

server.route({
    method: 'GET',
    path: '/developers/{version}/{context}/{topic}',
    handler: function (request, reply) {
        const params = request.params;

        reply.view(`developers/${params.version}/${params.context}/${params.topic}`, {
            nav: {
                primary: require('./data/nav/primary'),
                secondary: require('./data/nav/dev'),
                footer: require('./data/nav/footer'),
            },
        }, {
            layout: 'developer',
        });
    },
});

server.start((err) => {
    if (err) {
        throw err;
    }

    console.log(`Server running at: ${server.info.uri}`);
});
