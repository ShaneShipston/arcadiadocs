const gulp = require('gulp');
const sass = require('gulp-sass');
const prefix = require('gulp-autoprefixer');
const minifycss = require('gulp-clean-css');
const ttf2woff = require('gulp-ttf2woff');
const ttf2woff2 = require('gulp-ttf2woff2');
const imagemin = require('gulp-imagemin');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const otf2ttf = require('otf2ttf');
const sync = require('browser-sync').create();
const fs = require('fs');
const webpackconfig = Object.create(require('./webpack.config.js'));
const webpack = require('webpack');
const gutil = require('gulp-util');

const config = {
    url: 'localhost',
    port: 3000,
    src: {
        img: 'src/img/**/*.{png,jpg,gif}',
        js: 'src/js/**/*.js',
        css: 'src/scss/**/*.scss',
        fonts: 'src/fonts/',
    },
    dest: {
        img: 'public/img',
        js: 'public/js',
        css: 'public/css',
        fonts: 'public/fonts',
    },
    watch: [
        '**/*.html',
    ],
};

// --- [DEV TASKS] ---

gulp.task('sync', () => {
    sync.init({
        proxy: config.url,
        port: config.port,
        ui: false,
        online: true,
        logPrefix: 'Arcadia',
        open: false,
    });
});

gulp.task('watch', () => {
    // CSS
    watch(config.src.css, () => {
        gulp.start('styles');
    });

    // Images
    watch(config.src.img, { events: ['add'] })
        .pipe(imagemin())
        .pipe(gulp.dest(config.dest.img));

    // PHP
    watch(config.watch, () => {
        sync.reload();
    });

    // OTF to TTF
    watch(`${config.src.fonts}*.otf`, { events: ['add'] })
        .pipe(otf2ttf())
        .pipe(gulp.dest(config.src.fonts));

    // TTF to WOFF
    watch(`${config.src.fonts}*.ttf`, { events: ['add'] })
        .pipe(ttf2woff())
        .pipe(gulp.dest(config.dest.fonts));

    // TTF to WOFF2
    watch(`${config.src.fonts}*.ttf`, { events: ['add'] })
        .pipe(ttf2woff2())
        .pipe(gulp.dest(config.dest.fonts));

    // Append @font-face
    watch(`${config.src.fonts}*.ttf`, { events: ['add'] }, (file) => {
        fs.appendFile('src/scss/base/_fonts.scss', `@include fontface("${file.stem}", "${file.stem}");\n`);
    });

    // JS
    watch(config.src.js, () => {
        gulp.start('javascript');
    });
});

gulp.task('styles', () => {
    gulp.src(config.src.css)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
        }).on('error', sass.logError))
        .pipe(prefix({
            cascade: false,
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.dest.css))
        .pipe(sync.stream({
            match: '**/*.css',
        }));
});

gulp.task('images', () => {
    gulp.src(config.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(config.dest.img));
});

gulp.task('javascript', (callback) => {
    webpackconfig.plugins = webpackconfig.plugins = [
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
        }),
    ];

    webpack(webpackconfig, (err, stats) => {
        if (err) {
            throw new gutil.PluginError('webpack:build', err);
        }

        gutil.log('[webpack:build]', stats.toString({
            colors: true,
        }));

        sync.reload();

        callback();
    });
});

// --- [BUILD TASKS] ---

gulp.task('build', () => {
    gulp.src(config.src.css)
        .pipe(sass({
            outputStyle: 'compressed',
            errLogToConsole: true,
        }))
        .pipe(prefix({
            remove: false,
            cascade: false,
        }))
        .pipe(minifycss())
        .pipe(gulp.dest(config.dest.css));
});

gulp.task('default', ['sync', 'styles', 'javascript', 'images', 'watch']);
