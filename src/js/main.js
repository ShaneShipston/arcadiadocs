import SmoothScroll from './smoothscroll';

/**
 * Activate smooth scroll
 */
const scroll = new SmoothScroll();

/**
 * Add anchor link tracking
 */
const links = document.querySelectorAll('a[href*="#"]');

Array.from(links).forEach((element) => {
    let href = element.getAttribute('href');

    // Clean up URL
    if (href.indexOf('#') !== false) {
        href = href.substr(href.indexOf('#'));
    }

    if (href.length > 2) {
        element.addEventListener('click', () => {
            // Make sure the anchor is on the current page
            if (location.pathname.replace(/^\//, '') === element.pathname.replace(/^\//, '')
                && location.hostname === element.hostname) {
                scroll.to(href);
            }
        });
    }
});

/**
 * Check URL for hash
 */
window.addEventListener('load', () => {
    if (window.location.hash) {
        scroll.to(window.location.hash);
    }
});

/**
 * View Examples
 */
const exampleButtons = document.querySelectorAll('.view-example button');

if (exampleButtons) {
    Array.from(exampleButtons).forEach((button) => {
        button.addEventListener('click', () => {
            const method = button.closest('.method');
            const example = method.querySelector('.examples');

            example.classList.toggle('open');
        });
    });
}

/**
 * Sub Menu
 */
const navHeadings = document.querySelectorAll('nav.secondary .heading');

if (navHeadings) {
    Array.from(navHeadings).forEach((target) => {
        target.addEventListener('click', () => {
            target.classList.toggle('active');
            target.closest('li').querySelector('.sub-menu').classList.toggle('active');
        });
    });
}
