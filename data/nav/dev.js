module.exports = () => {
    const menuItems = [
        {
            heading: 'Getting Started',
            active: false,
            items: [
                {
                    path: '/developers/3.1/getting-started/installation',
                    title: 'Installation',
                    active: false,
                },
                {
                    path: '/developers/3.1/getting-started/creating-blocks',
                    title: 'Creating Blocks',
                    active: false,
                },
                {
                    path: '/developers/3.1/getting-started/managing-assets',
                    title: 'Managing Assets',
                    active: false,
                },
            ],
        },
        {
            heading: 'Classes',
            active: true,
            items: [
                {
                    path: '/developers/3.1/classes/banner',
                    title: 'Banner',
                    active: false,
                },
                {
                    path: '/developers/3.1/classes/field',
                    title: 'Field',
                    active: true,
                },
                {
                    path: '/developers/3.1/classes/layout',
                    title: 'Layout',
                    active: false,
                },
                {
                    path: '/developers/3.1/classes/loop',
                    title: 'Loop',
                    active: false,
                },
                {
                    path: '/developers/3.1/classes/opening-hours',
                    title: 'OpeningHours',
                    active: false,
                },
                {
                    path: '/developers/3.1/classes/store-hours',
                    title: 'StoreHours',
                    active: false,
                },
                {
                    path: '/developers/3.1/classes/schema',
                    title: 'Schema',
                    active: false,
                },
            ],
        },
        {
            heading: 'SASS',
            active: false,
            items: [
                {
                    path: '/developers/3.1/sass/mixins',
                    title: 'Mixins',
                    active: false,
                },
                {
                    path: '/developers/3.1/sass/functions',
                    title: 'Functions',
                    active: false,
                },
            ],
        },
        {
            heading: 'Guides',
            active: false,
            items: [
                {
                    path: '/developers/3.1/guide/custom-banner',
                    title: 'Custom Banner',
                    active: false,
                },
                {
                    path: '/developers/3.1/guide/creating-post-types',
                    title: 'Creating Post Types',
                    active: false,
                },
                {
                    path: '/developers/3.1/guide/production-ready',
                    title: 'Preparing for Production',
                    active: false,
                },
            ],
        },
    ];

    return menuItems;
};
