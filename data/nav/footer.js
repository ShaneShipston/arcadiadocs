module.exports = () => {
    const menuItems = [
        {
            path: '/changelog',
            title: 'Changelog',
            active: false,
        },
        {
            path: '/credits',
            title: 'Credits',
            active: false,
        },
    ];

    return menuItems;
};
