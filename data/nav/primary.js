module.exports = () => {
    const menuItems = [
        {
            path: '/',
            title: 'Home',
            active: false,
        },
        {
            path: '/documentation',
            title: 'Documentation',
            active: false,
        },
        {
            path: '/developers',
            title: 'Developers',
            active: false,
        },
        {
            path: '/faq',
            title: 'FAQ',
            active: false,
        },
        {
            path: '/support',
            title: 'Support',
            active: false,
        },
    ];

    return menuItems;
};
